%%%-------------------------------------------------------------------
%%% @author Damian T. Dobroczyński
%%% @copyright (C) 2013, Damian T. Dobroczyński
%%% @doc
%%%
%%% @end
%%% Created : 28 Aug 2013 by Damian T. Dobroczyński
%%%-------------------------------------------------------------------
-module (protocols_app).

-behaviour (application).

%% Application callbacks
-export ([start/2, stop/1]).

%%%===================================================================
%%% Application callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called whenever an application is started using
%% application:start/[1,2], and should start the processes of the
%% application. If the application is structured according to the OTP
%% design principles as a supervision tree, this means starting the
%% top supervisor of the tree.
%%
%% @spec start(StartType, StartArgs) -> {ok, Pid} |
%%                                      {ok, Pid, State} |
%%                                      {error, Reason}
%%      StartType = normal | {takeover, Node} | {failover, Node}
%%      StartArgs = term()
%% @end
%%--------------------------------------------------------------------

start (_StartType, _StartArgs) ->
  Result = {ok, _} = supervisor:start_link(protocols_sup, []),
  %% get default protocols & implementations
  Protos = case application:get_env(protocols, defimp) of
             undefined  -> [];
             {ok, List} -> List
           end,
  [protocols:addimp(Proto, Imps) || {Proto, Imps} <- Protos],
  Result.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called whenever an application has stopped. It
%% is intended to be the opposite of Module:start/2 and should do
%% any necessary cleaning up. The return value is ignored.
%%
%% @spec stop(State) -> void()
%% @end
%%--------------------------------------------------------------------

stop (_State) ->
  ok.
