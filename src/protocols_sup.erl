%%%-------------------------------------------------------------------
%%% @author Damian T. Dobroczyński
%%% @copyright (C) 2013, Damian T. Dobroczyński
%%% @doc
%%%
%%% @end
%%% Created : 28 Aug 2013 by Damian T. Dobroczyński
%%%-------------------------------------------------------------------
-module (protocols_sup).
-behaviour (supervisor).
%% Supervisor callbacks
-export ([init/1]).

%%%===================================================================
%%% Supervisor callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Whenever a supervisor is started using supervisor:start_link/[2,3],
%% this function is called by the new process to find out about
%% restart strategy, maximum restart frequency and child
%% specifications.
%%
%% @spec init(Args) -> {ok, {SupFlags, [ChildSpec]}} |
%%                     ignore |
%%                     {error, Reason}
%% @end
%%--------------------------------------------------------------------
init ([]) ->
  ets:new(protocols, [named_table,
                      public,
                      set,
                      {keypos, 1},
                      {write_concurrency, true}]),
  {ok, {{one_for_one, 1000, 3600}, []}}.
