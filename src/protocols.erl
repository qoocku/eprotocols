-module (protocols).
-export ([start/0,
          stop/0,
          addimp/2,
          implementation/3,
          implementations/1,
          apply/3,
          next/0,
          next/1,
          efficiency/4]).

start () ->
  application:start(protocols).

stop () ->
  application:stop(protocols).

addimp (Proto, Imp) when is_atom(Imp) ->
  {PExps, PFuns} = get_proto_features(Proto),
  addimp(Proto, PExps, PFuns, Imp);
addimp (Proto, Imps = [_|_]) ->
  {PExps, PFuns} = get_proto_features(Proto),
  addimp(Proto, PExps, PFuns, Imps).

get_proto_features (Proto) ->
  {Proto:module_info(exports),
   [{Fun, Arity} || {{Fun, Arity}, _} <- proplists:get_value(callback,
                                                             Proto:module_info(attributes), [])]}.

addimp (Proto, ProtoExports, ProtoFuns, [Imp|Imps]) ->
  Imp = addimp(Proto, ProtoExports, ProtoFuns, Imp),
  addimp(Proto, ProtoExports, ProtoFuns, Imps);
addimp (_, _, _, []) ->
  ok;
addimp (Proto, ProtoExports, ProtoFuns, Imp) ->
  ImpExports   = Imp:module_info(exports),
  case {ProtoFuns -- ProtoExports, ProtoFuns -- ImpExports} of
    {[], []} ->
      case ets:lookup(protocols, Proto) of
        [] ->
          true = ets:insert_new(protocols, {Proto, [Imp]});
        [{Proto, Imps}] ->
          true = ets:insert(protocols, {Proto, [Imp|lists:delete(Imp, Imps)]})
      end,
      Imp;
    {_,[]} ->
      error(undefined_default_implementation);
    {_, Left} ->
      error({missing_implementation, Left})
  end.

implementations (Proto) ->
  case ets:lookup(protocols, Proto) of
    [] ->
      error(undefined_implementation);
    [{Proto, Imps}] ->
      Imps
  end.

implementation (Proto, Fun, Args) ->
  Imps = implementations(Proto),
  protocols:apply(Imps, Fun, Args).

apply ([Imp|Imps], Fun, Args) ->
  case (catch erlang:apply(Imp, Fun, Args)) of
    {'$$$protocols-next'} ->
      ?MODULE:apply(Imps, Fun, Args);
    {'$$$protocols-next', Imp} ->
      ?MODULE:apply(Imps, Fun, Args);
    {'$$$protocols-next', OtherImp} ->
      ?MODULE:apply(lists:dropwhile(fun (M) when M =/= OtherImp -> true ; (_) -> true end, Imps), Fun, Args);
    {'EXIT', {function_clause, [{Imp, Fun, Args, _}|_]}} ->
      ?MODULE:apply(Imps, Fun, Args);
    Value -> Value
  end;
apply ([], _, _) ->
  error(missing_implementation).

next () ->
  {'$$$protocols-next'}.

next (LastMod) ->
  {'$$$protocols-next', LastMod}.


efficiency (N, Proto, Fun, Args) ->
  {Time, Result} = timer:tc(fun () -> efficiency_loop(N, fun () -> erlang:apply(Proto, Fun, Args) end, ok) end),
  {Time/1000000/N, Result}.

efficiency_loop (0, _, Result) ->
  Result;
efficiency_loop (N, Fun, _) ->
  efficiency_loop(N-1, Fun, Fun()).
