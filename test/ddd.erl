-module (ddd).
-behavior (aaa).

-export ([is_empty/1]).

is_empty (<<>>) ->
  true;
is_empty (B) when is_binary(B) ->
  false;
is_empty (_) ->
  protocols:next().


