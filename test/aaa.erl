-module (aaa).
-compile (export_all).

-callback is_empty (any()) -> boolean().

is_empty (N) when is_number(N) andalso N /= 2 ->
  N == 0;
is_empty (List) when is_list(List) ->
  List =:= [];
is_empty (Any) ->
  protocols:implementation(?MODULE, is_empty, [Any]).
