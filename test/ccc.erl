-module (ccc).
-behavior (aaa).
-export ([is_empty/1]).

is_empty (2) ->
  true;
is_empty ({qq, 3}) ->
  true;
is_empty (A) when is_atom(A) ->
  true;
is_empty (_) ->
  protocols:next().

